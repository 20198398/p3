def main():
    print("++Suma de números positivos++")
    print("*Esta suma se realiza hasta introducir un número negativo*")
    numero = int(input("-Digite un número: "))
    suma = 0
    while numero >= 0:
        suma += numero
        numero = int(input("-Digite otro número: "))
    print(f"La suma de los números positivos digitados es {suma}.")

if __name__ == "__main__":
    main()