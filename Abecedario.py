print("++Abecedario ordenado alfabético de forma descendente en mayúsculas++")

abece = ["a", "b", "c", "d", "h", "i", "j", "k","l","m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
abece_cap = list(map(str.upper, abece))
abece_cap.reverse()
print (abece_cap)

