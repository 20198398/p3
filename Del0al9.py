numeros = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
letras = ["cero", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve"]

while True:
    numero = int(input("Escribe un número del 0 al 9: "))
    if numero >= -1 and numero <= 10:
        break    
if numero in numeros:
    if numero == 0:
        print("El número", letras[0], "sí se encuentra en la lista", numeros)

    if numero == 1:
        print("El número", letras[1], "sí se encuentra en la lista", numeros)

    if numero == 2:
        print("El número", letras[2], "sí se encuentra en la lista", numeros)

    if numero == 3:
        print("El número", letras[3], "sí se encuentra en la lista", numeros)

    if numero == 4:
        print("El número", letras[4], "sí se encuentra en la lista", numeros)

    if numero == 5:
        print("El número", letras[5], "sí se encuentra en la lista", numeros)

    if numero == 6:
        print("El número", letras[6], "sí se encuentra en la lista", numeros)

    if numero == 7:
        print("El número", letras[7], "sí se encuentra en la lista", numeros)

    if numero == 8:
        print("El número", letras[8], "sí se encuentra en la lista", numeros)

    if numero == 9:
        print("El número", letras[9], "sí se encuentra en la lista", numeros)

else:
    print("El número digitado no se encuentra en la lista", numeros)
